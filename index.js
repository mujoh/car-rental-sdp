const express = require('express');
const bodyparser = require("body-parser");
const app = express();
const MongoClient = require('mongodb').MongoClient;
const jwt_secret = 'WU5CjF8fHxG40S2t7oyk';
const jwt_admin = 'SJwt25Wq62SFfjiw92sR';

var _MS_PER_DAY = 1000 * 60 * 60 * 24;
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var MongoId = require('mongodb').ObjectID;
var database;

app.use('/', express.static('static'));
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded({
    extended: true
})); // to support URL-encoded bodies

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

app.get('/car', function(request, response) {
    database.collection('cars').find().toArray((err, cars) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(cars);
    })
});

app.get("/car/:id", function(req, res){
    database.collection('cars').find({
        _id: new MongoId(req.params.id)
    }).toArray((err, doc) => {
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(doc);
    });
});

app.post('/order', function(req, res){
  req.body.state = "processing";
  req.body.order_date = new Date();
  var order = req.body;
  order.pickup_date = new Date(order.pickup_date);
  order.return_date = new Date(order.return_date);
  order.total_days = dateDiffInDays(order.pickup_date, order.return_date);

  database.collection('orders').insert(order, function(err, data) {
    if (err) return console.log(err);
    res.setHeader('Content-Type', 'application/json');
    res.send(order);
  })  
});

app.get('/manufacturer', function(request, response) {
    database.collection('manufacturers').find().toArray((err, manufacturers) => {
        if (err) return console.log(err);
        response.setHeader('Content-Type', 'application/json');
        response.send(manufacturers);
    })
});

app.post('/sendmail', function(request, response){
    var email = request.body;
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'jomurentals@gmail.com',
          pass: '00000000Mujo'
        }
      });
      
      var mailOptions = {
        from: email.email,
        to: 'jomurentals@gmail.com',
        subject: email.subject,
        text: 'Name: ' + email.name + '\n' + 'User Email: '+ email.email + '\n' + email.body
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            response.send({
                success : false,
                message : "Error in sending email"
            })
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
          response.send({
              success : true,
              message : "Email Sent!"
          });
        }
      });
});

app.post('/rentemail', function(request, response){
    var email = request.body;
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'jomurentals@gmail.com',
          pass: '00000000Mujo'
        }
      });
      
      var mailOptions = {
        from: "jomurentals@gmail.com",
        to: email.email,
        subject: "Rent Processed",
        html: 
        "<table>"+
        "<tr>"+
        "<td>Dear <strong>"+email.name+",</strong></td>" +
        "</tr>"+
        "<tr>" +
        "<td> Your rent is being processed by our employees. We will be in touch soon.</td>"+
        "</tr>"+
        "<tr>"+
        "<td> Regards,<br>CarRentalName </td>"+
        "</tr> <br>"+
        "<tr>"+
        "<td align='center' bgcolor='grey' style='padding: 40px 0 30px 0;'><img src='https://image.ibb.co/ctVA9J/logo.png'><br><div style='color: white; padding: 10px; margin-top: 20px;'>CarRentalName <br>Nikole Sopa 9 <br>062116767</div></td>"+
        "</table>"
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            response.send({
                success : false,
                message : "Error in sending email"
            })
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
          response.send({
              success : true,
              message : "Email Sent!"
          });
        }
      });
});

app.get('/pictures', function(req, res){
    database.collection('gallery').find().toArray((err, picture) => {
        if(err) return console.log(err);
        res.setHeader('Content-Type', 'application/json');
        res.send(picture);
    })
});

MongoClient.connect('mongodb://mujoh:062116767Mujo@ds155730.mlab.com:55730/carrental', function(err, client) {
    if (err) throw err;

    database = client.db('carrental');
    app.listen(process.env.PORT || 3000, () => console.log('Example app listening on port 3000!'))
});