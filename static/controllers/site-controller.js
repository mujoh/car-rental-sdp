function SiteController($scope, $http, $location, $route, toastr, $ngConfirm, $timeout, $anchorScroll, $routeParams, $filter, Popeye){
    console.log("Hello from Site Controller");

    refresh_manufacturers();
    refresh_pictures();
    refresh_cars();

    function refresh_cars(){
        $http.get('/car').then(function(res) {
                $scope.cars_list = res.data;

                $scope.order = {};
                $scope.order.car = $scope.cars_list[$routeParams.name];
            }),
            function(res) {
                alert(res.status);
            }
    };

    function rentemail(email){
        $http.post('/rentemail', email).then(function(response){
            //console.log("Sent!");
        }),function(error){
            console.log(error);
        }
    }

    $scope.check_login = function(){
        if(localStorage.getItem('user')){
            return true;
        }
        return false;
    }

    $scope.check_admin = function(){
        if(localStorage.getItem('type') == "admin"){
            return true;
        }
        return false;
    }

    $scope.login = function(credentials){
        $http.post('/login', credentials).then(function(response){
            if(typeof response.data.token != 'undefined'){
                localStorage.setItem('user',response.data.token)
                localStorage.setItem('type', response.data.type)
                toastr.success('You are successfully logged in!', 'Login Success!');
                $location.url('/');
            }
            else if(response.data.user == false){
                toastr.error('Login Error');
            }
        }),function(response){
            console.log(error);
        }
    }

    $scope.logout = function(){
        localStorage.clear();
        toastr.info("Successfully logged out!", "Logged Out!");
    }

    $scope.sendmail = function(email){
        $http.post('/sendmail', email).then(function(response){
            $route.reload();
            toastr.success('Email Sent!');
        }),function(error){
            console.log(error);
        }
    }

    function refresh_manufacturers() {
        $http.get('/manufacturer').then(function(res) {
                $scope.manufacturers_list = res.data;
            }),
            function(res) {
                alert(res.status);
            }
    };

    function refresh_pictures(){
        $http.get('/pictures').then(function(res){
            $scope.pictures_list = res.data;
        }),
        function(res){
            alert(res.status);
        }
    };

    $scope.getSingle = function(id){
        $http.get('/car/' + id).then(function(res) {
            $scope.car_info = res.data[0];
            //console.log($scope.car_info);
        })
    };

    $scope.openNavigationDrawer = function(){
        if ($scope.mobileNavigationOpen == 'nav-open'){
            $scope.mobileNavigationOpen = '';
        }else{
            $scope.mobileNavigationOpen = 'nav-open';
        }
        
    }
    
    function reset(){
        $scope.order = {};
        $scope.rent_a_car_form.$setPristine();
        $scope.rent_a_car_form.$setUntouched();
    }

    $scope.make_order = function(){
        $ngConfirm({
            title: "Confirm Order",
            content: "Do you really want to make this order?",
            type: 'orange',
            typeAnimated: true,
            icon: 'fa fa-success',
            scope: $scope,
            buttons: {
              yes: {
                text: "Yes",
                btnClass: 'btn-orange',
                action: function(scope, button){
                    $http.post('/order', $scope.order).then(function(data){
                        rentemail($scope.order);
                        toastr.success('Order is going to be processed!');
                        reset();
                        Popeye.closeCurrentModal();
                    })
                }   
              },
              close: function(scope, button){
                //Closes Modal
              }
            }
        })
    }

    $scope.menuItemClicked = function(){
        $scope.mobileNavigationOpen = '';
    }


    // set minimum date to yesterday
    $scope.minDateMoment = moment().subtract(1, 'day');
    $scope.minDateString = moment().subtract(1, 'day').format('YYYY-MM-DD');

    var car_name = $routeParams.name;

    $timeout(function() {
        $anchorScroll(car_name);
    });

    $scope.reset_filters = function(filter){
        filter.car_type = "";
        filter.car_doors = "";
        filter.car_transmission = "";
    }

    $scope.reset_price_range = function(slider){
        slider.minPrice = 1;
        slider.maxPrice = 500;
    }

    $scope.reset_all = function(filter, slider){
        $scope.reset_filters(filter);
        $scope.reset_price_range(slider);
    }

    $scope.byRange = function (fieldName, minValue, maxValue) {
        if (minValue === undefined) minValue = Number.MIN_VALUE;
        if (maxValue === undefined) maxValue = Number.MAX_VALUE;
        
        return function predicateFunc(item) {
          return minValue <= item[fieldName] && item[fieldName] <= maxValue;
        };
    };
    
    $scope.slider = {
        minPrice: 1,
        maxPrice: 500,
        options: {
            floor: 0,
            ceil: 500,
            step: 1
        }
    };

    function get_index(car_name, cars_list){
        //search value
        var car_to_search = car_name;

        //filter the array
        var foundItem = $filter('filter')(cars_list, { car_name: car_to_search  }, true)[0];

        //get the index
        var index = cars_list.indexOf(foundItem);

        return index;
    }

    
    $scope.rent_car = function(car_name, cars_list){
        $scope.index = get_index(car_name, cars_list);
        var modal = Popeye.openModal({
            templateUrl: "rent_a_car_modal.html",
            controller: "siteController"
        })
    }
}